---

## SCRUM BOARD

Scrum Board is designed as three layer architecture. 

1. Presentation Layer is developed in 
a. DJANGO (2.1.7)
b. MySQL
c. Python 3.6

2. Middle Layer avails end user with API Engine for 
a. Creating Sprint 
b. Update Sprint
c. Add Task
d. Update Task 

---